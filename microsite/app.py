import sys
import sqlite3
import json
from flask import Flask, Response, redirect, request
import mimetypes
import os

app = Flask(__name__)


def make_response(data, origin="*", status_code=200, mimetype="application/json"):
    response = Response(data, status=status_code, mimetype=mimetype)
    response.headers.add("Access-Control-Allow-Origin", origin)
    response.headers.add("Access-Control-Allow-Credentials", "true")
    return response


@app.route("/")
def index():
    html = open("index.html").read()
    return make_response(html, mimetype="text/html")


@app.route("/<path:path>")
def afile(path):
    _, filename = os.path.split(path)
    return make_response(
        open("./" + path, "rb").read(), mimetype=mimetypes.guess_type(filename)[0]
    )


@app.route("/api/term/<path:term>")
def api_term(term):
    DB = sqlite3.connect("CIT.sqlite3")
    cursor = DB.cursor()
    cursor.execute("SELECT * FROM terms WHERE term = ?", (term,))
    k, v = cursor.fetchone()
    return make_response(v)


if __name__ == "__main__":
    try:
        port = int(sys.argv[1])
    except Exception:
        print("First cmd line argument mut be a numeric port number")
        sys.exit(1)
    app.run(port=sys.argv[1])

