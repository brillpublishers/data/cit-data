# CIT

Chinese Iconography Thesaurus

https://chineseiconography.org/

The Chinese Iconographic Thesaurus (CIT) is a three-year research project (October 2016 - October 2019) funded by the UK Department of Digital Culture Media and Sport with its team based at the V&A Museum. It aims to create indexing standards that will facilitate access and interoperability of Chinese digital images across collections. After learned from previous works (e.g. IconClass and AAT), this project presents a unique opportunity to create an alternative classification scheme deeply rooted in the specificity of Chinese art, with the potential to foster dialogue between the studies of Chinese art and European art. It is a multidisciplinary pilot project that brings together sinology, art history, and digital humanities to create the first thesaurus of Chinese iconography. CIT will be a valuable research tool that enhances the accessibility and understanding of Chinese art collections.